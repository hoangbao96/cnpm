@extends('master')
@section('content')
<div class="inner-header">
		<div class="container">
			<div class="pull-left">
				<h6 class="inner-title">Đặt Hàng</h6>
			</div>
			<div class="pull-right">
				<div class="beta-breadcrumb">
					<a href="{{route('trangchu')}}">Trang Chủ</a> / <span>Đặt Hàng</span>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	
	<div class="container">
		<div id="content">
			
			<form action="{{route('dathang')}}" method="post" class="beta-form-checkout">
				<input type="hidden" name ="_token" value="{{csrf_token()}}" >
				<div class="row">
					<div class="col-sm-6">
						<h4>Đơn Hàng</h4>
						<div class="space20">&nbsp;</div>

						<div class="form-block">
							<label for="your_first_name">Họ Tên*</label>
							<input type="text" id="your_first_name" name="name"placeholder="nguyen van a" required>
						</div>
						<div class="form-block">
							<label for="gender">Giới tính</label>
							
							<input id="gender" type="radio" class="input-radio" style="width: 10px" name="gender" value="nam" >
							<span style="margin-right: 10px ">Nam</span>
							<input id="gender" type="radio" class="input-radio" style="width: 10px" name ="gender" value="nữ" ><span>Nữ</span>
							
						</div>
						<div class="form-block">
							<label for="adress">Địa Chỉ*</label>
							<input type="text"  name="address" id="address"  required>
							
						</div>

						<div class="form-block">
							<label for="email">Email*</label>
							<input type="email" name="email" id="email" placeholder="abc@xyz.com" required>
						</div>

						<div class="form-block">
							<label for="phone">Số Điện Thoại*</label>
							<input type="text" id="phone" name="phone" placeholder="01264789" required>
						</div>
						
						<div class="form-block">
							<label for="notes">Ghi Chú</label>
							<textarea id="notes" name="note"></textarea>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="your-order">
							<div class="your-order-head"><h5>Giỏ Hàng</h5></div>
							<div class="your-order-body">
								<div class="your-order-item">
									<div>
									<!--  one item	 -->
									@if(Session::has('cart'))
									@foreach(Session::get('cart')->items as $product)
										<div class="media">
											<img width="35%" src="source/image/products/{{$product['item']['image']}}" alt="" class="pull-left">
											<div class="media-body">
												<p class="font-large">{{$product['item']['name']}}</p>
												<span class="color-gray your-order-info">Số lượng: {{$product['qty']}}</span>
											</div>
										</div>
									@endforeach
									@endif
									<!-- end one item -->
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="your-order-item">
									<div class="pull-left"><p class="your-order-f18">Tổng tiền:</p></div>
									<div class="pull-right">
									<h5 class="color-black">
									@if(Session::has('cart'))
									{{Session::get('cart')->totalPrice}} VNĐ
									@else
									0 VNĐ
									@endif
									</h5></div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="your-order-head"><h5>Phương thức thanh toán</h5></div>
							
							<div class="your-order-body">
								<ul class="payment_methods methods">
									<li class="payment_method_bacs">
										<input id="payment_method_bacs" type="radio" class="input-radio" name="payment_method" value="bacs" checked="checked" data-order_button_text="">
										<label for="payment_method_bacs">Thanh toán khi nhận hàng </label>
													
									</li>

									<li class="payment_method_cheque">
										<input id="payment_method_cheque" type="radio" class="input-radio" name="payment_method" value="cheque" data-order_button_text="">
										<label for="payment_method_cheque">Cheque Payment </label>
										<div class="payment_box payment_method_cheque" style="display: none;">
											Please send your cheque to Store Name, Store Street, Store Town, Store State / County, Store Postcode.
										</div>						
									</li>
									
									
								</ul>
							</div>

							<div class="text-center"><button type="submit" class="beta-btn primary" href="">Đặt Hàng <i class="fa fa-chevron-right"></i></button></div>
						</div> <!-- .your-order -->
					</div>
				</div>
			</form>
		</div> <!-- #content -->
	</div> <!-- .container -->
@endsection