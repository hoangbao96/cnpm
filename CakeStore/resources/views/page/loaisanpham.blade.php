@extends('master')
@section('content')
<div class="inner-header">
		<div class="container">
			<div class="pull-left">
				<h6 class="inner-title"> {{$productType->name}}</h6>
			</div>
			<div class="pull-right">
				<div class="beta-breadcrumb font-large">
					<a href="{{route('trangchu')}}">Home</a> / <span>Sản phẩm</span>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="container">
		<div id="content" class="space-top-none">
			<div class="main-content">
				<div class="space60">&nbsp;</div>
				<div class="row">
					<div class="col-sm-3">
						<ul class="aside-menu">
							@foreach($all_type as $type)
							<li ><a style="font-size: 15px" href="{{route('loaisanpham',$type->id)}}">{{$type->name}}</a></li>
							@endforeach
						</ul>
					</div>
					<div class="col-sm-9">
						<div class="beta-products-list">
							<h4>New Products</h4>
							<div class="beta-products-details">
								<p class="pull-left">Có {{count($products)}} sản phẩm</p>
								<div class="clearfix"></div>
							</div>
							
							<div class="row">
								@foreach($products as $pr )
								<div class="col-sm-4">
									<div class="single-item">
										@if($pr->promotion_price!=0)
											<div class="ribbon-wrapper">
												<div class="ribbon sale">Sale</div>
											</div>		
										@endif	
										<div class="single-item-header">
											<a href="{{route('chi-tiet-san-pham',$pr)}}"><img src="../source/image/products/{{$pr->image}}" height="250px" alt=""></a>
										</div>
										<div class="single-item-body">
											<p class="single-item-title">{{$pr->name}}</p>
											<p class="single-item-price" style="font-size: 18px">
												@if($pr->promotion_price==0)
												<span class="flash-sale">{{$pr->unit_price}} VNĐ</span>
												@else											
												<span class="flash-del">{{$pr->promotion_price}} VNĐ
												</span>
												<span class="flash-sale">{{$pr->unit_price}} VNĐ</span>
												@endif
											</p>
										</div>
										<div class="single-item-caption">
											<a class="add-to-cart pull-left" href="{{route('themvaogiohang',$pr->id)}}"><i class="fa fa-shopping-cart"></i></a>
											<a class="beta-btn primary" href="{{route('chi-tiet-san-pham',$pr->id)}}">Chi Tiết <i class="fa fa-chevron-right"></i></a>
											<div class="clearfix"></div>
										</div>
									</div>
								</div>
								@endforeach
							</div>
							<div class="row">
								{{$products->links()}}
							</div>
						</div> <!-- .beta-products-list -->

						<div class="space50">&nbsp;</div>

						<div class="beta-products-list">
							<h4>Sản phẩm khác</h4>
							<div class="beta-products-details">
								<p class="pull-left">Có {{count($other_products)}} sản phẩm khác</p>
								<div class="clearfix"></div>
							</div>
							<div class="row">
								@foreach($other_products as $op)
								<div class="col-sm-4">

									<div class="single-item">
										@if($pr->promotion_price!=0)
											<div class="ribbon-wrapper">
												<div class="ribbon sale">Sale</div>
											</div>		
										@endif											

										<div class="single-item-header">
											<a href="{{route('chi-tiet-san-pham',$op->id)}}"><img src="../source/image/products/{{$op->image}}" height="250px" alt=""></a>
										</div>
										<div class="single-item-body">
											<p class="single-item-title">{{$op->name}}</p>
											<p class="single-item-price" style="font-size: 18px">
												@if($pr->promotion_price==0)
												<span class="flash-sale">{{$op->unit_price}} VNĐ</span>
												@else											
												<span class="flash-del">{{$op->promotion_price}} VNĐ
												</span>
												<span class="flash-sale">{{$op->unit_price}} VNĐ</span>
												@endif
											</p>
										</div>
										<div class="single-item-caption">
											<a class="add-to-cart pull-left" href="{{route('themvaogiohang',$op->id)}}"><i class="fa fa-shopping-cart"></i></a>
											<a class="beta-btn primary" href="{{route('chi-tiet-san-pham',$op->id)}}">Chi Tiết <i class="fa fa-chevron-right"></i></a>
											<div class="clearfix"></div>
										</div>
									</div>
								</div>
								@endforeach
							</div>
							<div class="row">
								{{$other_products->links()}}
							</div>
							<div class="space40">&nbsp;</div>
							
						</div> <!-- .beta-products-list -->
					</div>
				</div> <!-- end section with sidebar and main content -->


			</div> <!-- .main-content -->
		</div> <!-- #content -->
	</div> <!-- .container -->
@endsection