<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('master');
});
Route::get('/index','pageController@getIndex')->name('trangchu');
Route::get('/loai-san-pham/{type}','pageController@getLoaiSanPham')->name('loaisanpham');
Route::get('/chi-tiet-san-pham/{id}','pageController@getChiTietSanPham')->name('chi-tiet-san-pham');
Route::get('/lien-he','pageController@getLienHe')->name('lienhe');
Route::get('/gioi-thieu','pageController@getGioiThieu')->name('gioithieu');
//Route::get('/loai-san-pham','pageController@getLoaiSanPham')->name('loaisanpham');
Route::get('/product-type/{type}','pageController@getType');
Route::get('add-to-cart/{id}','pageController@getAddToCart')->name('themvaogiohang');
Route::get('remove-product-from-cart/{id}','pageController@removeFromCart')->name('xoasanpham');
Route::get('dat-hang','pageController@getDatHang')->name('dathang');
Route::post('dat-hang','pageController@postDatHang')->name('chapnhandathang');
Route::get('dang-ky','pageController@getDangKy')->name('dangky');
Route::get('dang-nhap','pageController@getDangNhap')->name('dangnhap');
Route::post('dang-nhap','pageController@postDangNhap')->name('dangnhap');
Route::post('dang-ky','pageController@postDangKy')->name('dangky');
Route::get('dang-xuat','pageController@postDangXuat')->name('dangxuat');
Route::get('tim-kiem','pageController@postTimKiem')->name('timkiem');