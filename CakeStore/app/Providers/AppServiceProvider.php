<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\ProductType;
use App\Cart;
use Session;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //

        view()->composer('header',function($view){
            $loaiSp = ProductType::all();
            $view->with('loaiSp',$loaiSp);
        });
        view()->composer('header',function($view){
            if(Session('cart')){
                $oldCart = Session::get('cart');
                //$cart = new Cart($oldCart);
                $view->with(['totalPrice'=>$oldCart->totalPrice,'totalQty'=>$oldCart->totalQty]);
            }
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
