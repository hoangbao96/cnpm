<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $table = "products";
    public function productType(){
    	return $this->belongsTo('App/ProductType','id_type','id');
    }
    //doi voi belong to id nay la id cua product
    public function billDetail(){
    	return $this->hasMany('App/Bill','id_product','id');
    } 
}
