<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slide;
use App\Product;
use App\ProductType;
use App\Cart;
use App\Customer;
use App\Bill;
use App\BillDetail;
use App\User;
use Hash;
use Session;
use Auth;
class pageController extends Controller
{
    //

   
    public function getIndex(){
    	$slide = Slide::all();
    	$newProducts = Product::where('newproducts',1)->paginate(4);
    	$discountedProducts = Product::where('promotion_price','<>',0)->paginate(8);
    	return view('page.trangchu',['slide'=>$slide,'newProducts'=>$newProducts,'discountedProducts'=>$discountedProducts]);//'slide' la bien truyen vao view, $slide la bien o day

    }
     public function getChiTietSanPham(Request $req){
       $product_detail = Product::where('id',$req->id)->first();
        $related_products = Product::where('id_type',$product_detail->id_type)->paginate(6);
    	return view('page.chitietsanpham',compact('product_detail','related_products'));
    }
     public function getLienHe(){
    	
    }
    
     public function getLoaiSanPham($type){
     	$products = Product::where('id_type',$type)->paginate(3);
     	$other_products = Product::where('id_type','<>',$type)->paginate(3);
     	$all_type = ProductType::all();
     	$productType = ProductType::where('id','=',$type)->first();
        
    	return view('page.loaisanpham',compact('products','other_products','all_type','productType'));
    }
    public function getGioiThieu(){
    	
    }
    public function getAddToCart(Request $req,$id){//id cua san pham dc gui den tu trang chu

        $product = Product::find($id);// tim id trong database
        $oldCart = Session('cart')?Session::get('cart'):new Cart(null);// xem tinh trang gio hang hien tai null neu rong, session la de khi reload trang web thi gio hang van ton tai
       // $cart = new Cart($oldCart);// tao 1 doi tuong gio hang (dua tren gio hang hien tai)
        $oldCart->add($product,$id); // them gio hang vao cart
        $req->session()->put('cart',$oldCart);// thay cart trong session bang cart moi co them san pham moi
            
        return redirect()->back();// quay lai trang trc



    }
    public function removeFromCart(Request $req,$id1){
          //  $product = Product::find($id);
            $oldCart = Session('cart')?Session::get('cart'):new Cart(null);
            $oldCart->removeItem($id1);
            
            if(count($oldCart->items)>0){
                $req->session()->put('cart',$oldCart);
            }
            else{
                Session::forget('cart');
                return redirect()->route('trangchu');
            }
            return redirect()->back();

    }   
    public function getDatHang(){
        return view('page.dathang');
    }
    public function postDatHang(Request $req){
        $cart = Session::get('cart');
        $customer = new Customer;
        $customer->name = $req->name;
        $customer->gender = $req->gender;
        $customer->email = $req->email;
        $customer->address = $req->address;
        $customer->phone_number = $req->phone;
        $customer->note = $req->note;
        $customer->save();
        $bill = new Bill;
        $bill->id_customer = $customer->id;
        $bill->date_order = date('Y-m-d');
        $bill->total =$cart->totalPrice;
        $bill->payment = $req->payment_method;
        $bill->note = $req->note;
        $bill->save();

        
        foreach ($cart->items as $key => $value) {
            # code...
            $billDetail = new BillDetail;
            $billDetail->id_bill = $bill->id;
            $billDetail->id_product = $key;
            $billDetail->quantity = $value['qty'];
            $billDetail->unit_price = ($value['price']/$value['qty']);
            $billDetail->save();
        }
        Session::forget('cart');
        return redirect()->back()->with('thongbao','Đặt hàng thành công');
    }
    public function getDangKy(){
        return view('page.signup');
    }
    public function getDangNhap(){
        return view('page.dangnhap');
    }
    public function postDangKy(Request $req){
        $this->validate($req,
            [
                'email'=>'required|email|unique:users,email',
                'password'=>'required|min:6|max:20',
                'name'=>'required',
                'repassword'=>'required|same:password',


            ],
            [
                'email.required'=>'Vui lòng nhập email',
                'email.email'=>'Không đúng định dạng email',
                'email.unique'=>'Email đã được đăng ký bởi tài khoản khác',
                'password.required'=>'Vui lòng nhập mật khẩu',
                'repassword.same'=>'Mật khẩu không trùng nhau',
                'password.min'=>'Mật khẩu phải có ít nhất 6 ký tự',
                'password.max'=>'Mật khẩu tối đa có 20 ký tự',
                'name.required'=>'Hãy nhập họ tên của bạn'
            ]
        );
        $user = new User;
        $user->full_name = $req->name;
        $user->email = $req->email;
        $user->password = Hash::make($req->password);
        $user->phone = $req->phone;
        $user->address = $req->address;
        $user->save();
        return redirect()->back()->with('thanhcong','Tạo tài khoản thành công');

    }
    public function postDangNhap(Request $req){
        $this->validate($req,
            [
                'email'=>'required|email',
                'password' =>'required'
            ],
            [
                'email.required'=>'Vui lòng nhập email',
                'email.email'=>'Không đúng định dạng email',
                'password.required'=>'Vui lòng nhập mật khẩu',
            ]);
        $credentials = array('email'=>$req->email,'password'=>$req->password);
        if(Auth::attempt($credentials)){
            return redirect()->back()->with(['flag'=>'success','message'=>'Dang nhap thanh cong']);
            //return redirect()->back()->with(['flag'=>'danger','message'=>'Dang nhap khong thanh cong']);
        }
        else{
            return redirect()->back()->with(['flag'=>'danger','message'=>'Dang nhap khong thanh cong']);
        }
        
    }
    public function postDangXuat(){
        Auth::logout();
        return redirect()->route('trangchu');
    }
    public function postTimKiem(Request $req){
        $products = Product::where('name','like','%'.$req->search_name.'%')
                            ->orWhere('unit_price',$req->search_name)
                            ->get();
        return view('page.search',compact('products'));
    }
}
