<?php
namespace App;
class Cart{	
	public $items = null;//1 mang chu cac san pham va so luong, gia tuong ung
						 // items la 1 mang , ma moi phan tu lai la 1 associative array(hashmap) chua
						// san pham , so luong va gia tuong ung 
						// items=[	[banhngot,100000vnd,2],
						//			[banhmat,20000vnd,3],
						//	
						//			],$id la chi so cua mang $items
	public $totalQty = 0;
	public $totalPrice = 0;
	/*  constructor neu $oldcart se dc lay trong session, bang null neu gio hang chua co gi*/
	public function __construct($oldCart){
		if($oldCart){
			$this->items = $oldCart->items;
			$this->totalQty = $oldCart->totalQty;
			$this->totalPrice = $oldCart->totalPrice;
		}
	}

	public function add($item, $id,$qty=0){//$item o day la $product cu the, id la id cua product trong db
		if($item->promotion_price==0){
			$price= $item->unit_price;
		}
		else  $price = $item->promotion_price;
		$giohang = ['qty'=>0,'price'=>$price,'item'=>$item] ;// khoi tao 1 mat hang
		if($this->items){ // neu items khac rong, tuc la da co san pham trong roi
			if(array_key_exists($id, $this->items)){ // kiem tra san pham them vao da co trong items chua
				$giohang  = $this->items[$id];
				// toi uu cau lenh nay $giohang['qty'] = $this->$item[$id]['qty'];
			}
		}
		$giohang['qty']++;
		$giohang['price'] = $giohang['qty']*$price;
		$this->items[$id] = $giohang;
		$this->totalQty++;
		$this->totalPrice += $price;

	}
	public function reduceByOne($id){
		$this->items[$id]['qty']--;
		$this->items[$id]['price'] -= $this->items[$id]['item']['price'];
		$this->totalQty--;
		$this->totalPrice-=$this->items[$id]['item']['price'];
		if($this->items[$id]['qty']<=0){
			unset($this->items[$id]);
		}
	}
	public function removeItem($id){
		$this->totalQty -= $this->items[$id]['qty'];
		$this->totalPrice -= $this->items[$id]['price'];
		unset($this->items[$id]);
	}
}